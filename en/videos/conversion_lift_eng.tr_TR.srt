﻿1
00:00:02,051 --> 00:00:03,455
Dönüşüm Yükselişi'yle,

2
00:00:03,455 --> 00:00:07,044
Facebook Reklamlarınızın internetteki ve internet dışındaki

3
00:00:07,044 --> 00:00:09,217
etkisini kesin olarak ölçebilirsiniz.

4
00:00:09,217 --> 00:00:12,198
Dönüşüm Yükselişi'ni, işletmelerin reklam harcamalarının

5
00:00:12,198 --> 00:00:14,143
getirisini anlamasına ve pazarlama kanallarını daha etkili kampanyalar

6
00:00:14,143 --> 00:00:18,784
için optimize etmesine yardımcı olmak amacıyla oluşturduk.

7
00:00:18,784 --> 00:00:20,329
Özelliğin kullanımı son derece basit.

8
00:00:20,329 --> 00:00:23,862
Pazarlamacı olarak, insanlara farklı mecralarda erişmek istiyorsunuz.

9
00:00:23,862 --> 00:00:26,992
Bizim amacımız ise doğru kişilere Facebook'ta etkileşimde

10
00:00:26,992 --> 00:00:29,408
oldukları yerde yani mobil cihazlarında ve gittikleri

11
00:00:29,408 --> 00:00:33,781
yerlerde erişmenize yardımcı olacak

12
00:00:33,781 --> 00:00:35,388
vazgeçilmez bir parça olmak.

13
00:00:36,462 --> 00:00:40,516
Şimdi size Facebook reklamlarını kampanyalarınıza eklediğinizde,

14
00:00:40,516 --> 00:00:45,260
eklememenize kıyasla iş hacminizde nasıl artış yaşayacağınızı gösterebiliriz.

15
00:00:45,260 --> 00:00:48,762
Facebook Dönüşüm Yükselişi, reklamlarınızın

16
00:00:48,762 --> 00:00:50,720
ne kadar etkili olduğunu ve

17
00:00:50,720 --> 00:00:53,042
kazandığınız ek iş hacmini ölçmek için test ve

18
00:00:53,042 --> 00:00:55,630
kontrol gruplarından oluşan bir yöntem kullanır.

19
00:00:56,678 --> 00:00:58,101
Özellik şu şekilde çalışır.

20
00:00:58,101 --> 00:01:00,144
Facebook'ta bir kampanya yayınladığınızda

21
00:01:00,144 --> 00:01:03,460
hedef kitlenizi rastgele olarak reklamınızı gören ve

22
00:01:03,460 --> 00:01:06,646
görmeyen kişilerden oluşan iki gruba ayırırız.

23
00:01:07,646 --> 00:01:09,204
Kampanyanızın yayınlandığı süre boyunca

24
00:01:09,204 --> 00:01:11,568
dönüşüm verilerinizi Facebook'la paylaşabilirsiniz.

25
00:01:11,568 --> 00:01:14,507
Bunlar, internet sitenizden veya mobil uygulamanızdan satışlar ya da

26
00:01:14,507 --> 00:01:19,765
internet dışındaki satışlarınızla bağlantılı e-postalar veya telefon numaraları olabilir.

27
00:01:19,765 --> 00:01:22,638
Biz de, reklamınızı gören grupla

28
00:01:22,638 --> 00:01:25,748
görmeyen grubu karşılaştırarak

29
00:01:25,748 --> 00:01:28,018
dönüşümlerinizdeki yükselişi hesaplarız.

30
00:01:28,018 --> 00:01:31,786
Facebook reklamlarınızın etkisini gösteren bilgilere sahip olduğunuzda,

31
00:01:31,786 --> 00:01:34,778
hangi pazarlama taktiklerinin en çok işe yaradığını ve

32
00:01:34,778 --> 00:01:37,436
kampanyanızın hedeflerine ulaşıp ulaşmadığını görebilir

33
00:01:37,436 --> 00:01:40,624
böylece bilgiye dayalı kararlar alabilirsiniz.

34
00:01:40,624 --> 00:01:43,907
Özelliği kullanmaya başlamak için hesap temsilcinize ulaşın.

